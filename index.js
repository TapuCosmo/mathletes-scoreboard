"use strict";

const packageJSON = require("./package.json");

console.log(`Copyright (C) Vietbao Tran 2019-2020
Licensed under the open-source MIT license.
Available at https://gitlab.com/TapuCosmo/mathletes-scoreboard.
Feel free to report issues and suggest improvements.
Version ${packageJSON.version}
`);

const http = require("http");
const path = require("path");

const express = require("express");
const NestDB = require("nestdb");

const config = process.pkg ?
  require(path.join(path.dirname(process.execPath), "config.json")) :
  require("./config.json");

const sockets = [];
let competitions;

config.schools = config.schools.map(s => s.replace(/\./g, "&#46"));

const db = new NestDB({
  filename: process.pkg ?
    path.join(path.dirname(process.execPath), "data/competitions.db") :
    path.join(__dirname, "data/competitions.db")
});

const app = express();
const httpServer = http.createServer(app);
const io = require("socket.io")(httpServer);

function emitToAll(...args) {
  sockets.forEach(s => s.emit(...args));
}

app.disable("x-powered-by");

app.use("/display", express.static(path.join(__dirname, "display/dist")));
app.use("/data-editor", express.static(path.join(__dirname, "data-editor/dist")));

app.get("/competitions", (req, res) => {
  if (competitions) {
    return res.json(competitions);
  }
  res.status(500).send();
});

io.on("connection", socket => {
  sockets.push(socket);

  socket.on("disconnect", () => {
    sockets.splice(sockets.indexOf(socket), 1);
  });

  socket.on("getCompetitions", callback => {
    if (competitions) {
      return callback(null, competitions);
    }
    callback("Competitions not loaded on server");
  });

  socket.on("createCompetition", (competitionName, callback) => {
    const schools = {};
    config.schools.forEach(school => {
      schools[school] = {
        teams: {
          a: {
            arithmetic: null,
            trigonometry: null,
            geometry: null,
            algebra: null,
            group: null
          },
          b: {
            arithmetic: null,
            trigonometry: null,
            geometry: null,
            algebra: null,
            group: null
          }
        }
      };
    });
    const doc = {
      name: competitionName,
      schools: schools
    };
    db.insert(doc, (e, result) => {
      if (e) return callback(e.message);
      competitions.push(result);
      emitToAll("competitionCreate", result);
      callback(null, true);
    });
  });

  socket.on("updateCompetition", (competitionData, callback) => {
    db.update(
      {
        _id: competitionData._id
      },
      competitionData,
      {
        returnUpdatedDocs: true
      },
      (e, num, updated) => {
        if (e) return callback(e.message);
        const index = competitions.findIndex(c => c.name === updated.name);
        if (index >= 0) {
          competitions[index] = updated;
        }
        emitToAll("competitionUpdate", updated);
        callback(null, true);
      }
    );
  });

  socket.on("setTimerDuration", duration => {
    emitToAll("timerDurationUpdate", duration);
  });

  socket.on("setTimerState", state => {
    emitToAll("timerStateUpdate", state);
  });
});

db.load(e => {
  if (e) {
    console.error(e);
    process.exit();
  }
  console.log("Database loaded");
  db.find({}, (err, docs) => {
    if (err) return console.error(err);
    competitions = docs;
  });
  httpServer.listen(config.serverPort, () => {
    console.log(`Server ready on :${config.serverPort} at ${Date.now()}`);
  });
});
