/* globals io:false */

import Vue from "vue";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = false;

// Only use "default" competition for now
// TODO: Support multiple competitions

const socket = window.socket = io();

socket.on("connect", () => {
  console.log(`Websocket connected at ${Date.now()}`);
  socket.emit("getCompetitions", (e, competitions) => {
    if (e) return console.error(e);
    const competition = competitions.find(c => c.name === "default");
    if (competition) {
      store.commit("setCompetitionData", competition);
    } else {
      socket.emit("createCompetition", "default", () => {
        console.log("Created default competition");
      });
    }
  });
});

socket.on("competitionCreate", competitionData => {
  if (competitionData.name === "default") {
    store.commit("setCompetitionData", competitionData);
  }
});

socket.on("competitionUpdate", updatedData => {
  if (updatedData.name === "default") {
    store.commit("setCompetitionData", updatedData);
  }
});

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
