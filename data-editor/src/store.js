import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    competition: {
      schools: {}
    }
  },
  mutations: {
    setCompetitionData(state, competitionData) {
      state.competition = competitionData;
    },
    setSubjectData(state, {school, team, subject, value}) {
      console.log(state, school, team, subject, value);
      state.competition.schools[school].teams[team][subject] = value;
    }
  }
});
