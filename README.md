# Mathletes Scoreboard

Created by Vietbao Tran, president of the Cosumnes Oaks High School Mathletes Team from 2018-2020.

In memory of my school's team's greatest-performing year that was abruptly ended by the COVID-19 pandemic.

## Feature Requests and Bug Reports

Please create an issue here to suggest improvements or report bugs: https://gitlab.com/TapuCosmo/mathletes-scoreboard/issues.

## Screenshots

![Scoreboard Display](https://i.imgur.com/aDcFxYZ.png)

![Data Editor](https://i.imgur.com/V163mcB.png)

## Caution

The security of this application is as strong as your hotspot's security.
Choose a strong password for your hotspot.
It is highly recommended to disconnect all computers used in the instructions from all
other Internet access points while the web server is in use to prevent unauthorized access
to the data editor.

It is also highly suggested to set up this application ahead of time and have users
practice using the editor interface to avoid technical and user issues at the competition.

## Instructions

1. Download the latest release for your operating system from
https://gitlab.com/TapuCosmo/mathletes-scoreboard/-/releases and extract it.
2. Edit `config.json` to set the port you want the application to run on (default: 8350),
and set the list of schools attending the competition.
3. Open a terminal window and use it to execute the executable file to start the web server.
You may need to add the execute permission to the file. Double clicking the executable may cause
issues due to it opening in the background on certain operating systems.
4. Connect your computer to a phone's (password-protected) hotspot (or make a hotspot
on your computer if it has that functionality).
5. Find your computer's local IP address. [See this link for instructions.](https://kb.iu.edu/d/aapa)
6. Skip this step if you plan to use the computer running the web server to access the data editor.
Connect the computer that you would like to use as the data editor to the hotspot.
7. On the data editor's computer, go to `http://<insert IP from step 5 here>:<port>/data-editor`.
(Example: http://10.16.1.30:8350/data-editor)
8. If there are any data remaining from a previous competition, remove the data and click the update scores button.
Alternatively, close the application, delete the `data` folder, and start the application again, then refresh
the data editing page.
9. Connect the computer that you would like to use as the data display to the hotspot.
This computer should be connected to a projector for public viewing.
10. On the data display's computer, go to `http://<insert IP from step 5 here>:<port>/display`.
(Example: http://10.16.1.30:8350/display)
11. Press Fn + F11 to make the data display full screen. Press Ctrl + Plus or Ctrl + Minus to zoom in and out as needed.
12. To update scores, type in the scores in the appropriate places in the data editor and click the update scores button.
13. To use the timer, enter in the amount of time and click the reset button, then click the start timer
button once you want to start the timer. To reset the timer once it is done, click the reset button.
14. Close the web server after the competition is over.

### Multiple Data Editors

It is possible to have more than one data editor. This can be useful if there is
one person controlling the timer and one person updating scores. However, each person
must be careful to not use the functionality that the other person is using, as
it can cause data loss! For example, if the person assigned to the timer clicks the
update scores button, all unsaved scores are deleted from the data editor belonging
to the score updater. There MUST NOT be more than one score updater.

## Running on Android

It is possible to run this application on an Android device that can create a hotspot.
It requires installing Termux and installing Node.js within Termux. You can then build
this application in Termux. This method is not recommended.

## Building For Local Use

1. `git clone https://gitlab.com/TapuCosmo/mathletes-scoreboard.git && cd mathletes-scoreboard`
2. `npm i`
3. `npm run build-all`
4. Edit `config.json` to your liking.
5. To start the web server: `npm start`

## Building For Release
1. `git clone https://gitlab.com/TapuCosmo/mathletes-scoreboard.git && cd mathletes-scoreboard`
2. `npm i`
3. `npm run build-release`
