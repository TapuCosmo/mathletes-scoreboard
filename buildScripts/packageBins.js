"use strict";

/* eslint-disable no-sync */

const fs = require("fs");
const path = require("path");

const Zip = require("node-zip");

console.log("Beginning to package bins...");

const configFile = fs.readFileSync(path.join(__dirname, "../config.json"));
const readmeFile = fs.readFileSync(path.join(__dirname, "../README.md"));
const licenseFile = fs.readFileSync(path.join(__dirname, "../LICENSE.md"));

const binPath = path.join(__dirname, "../bin");
const binFiles = fs.readdirSync(binPath);
fs.mkdirSync(path.join(binPath, "zip"));
for (const binFileName of binFiles) {
  const zip = new Zip();
  const binFile = fs.readFileSync(path.join(binPath, binFileName));
  zip.file(binFileName, binFile, {
    unixPermissions: "755"
  });
  zip.file("config.json", configFile);
  zip.file("README.md", readmeFile);
  zip.file("LICENSE.md", licenseFile);
  const outputZip = zip.generate({
    base64: false,
    compression: "DEFLATE"
  });
  fs.writeFileSync(path.join(binPath, "zip", `${binFileName}.zip`), outputZip, "binary");
  console.log(`Packaged ${binFileName}`);
}

console.log("Finished packaging all bins.");
